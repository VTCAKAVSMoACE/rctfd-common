# Modified from: https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Rust.gitlab-ci.yml
# If you aren't making a Rust repository (such as meta repositories), don't use
# this repository.

# This file is a template, and might need editing before it works on your project.
# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/rust/tags/
image: "registry.gitlab.com/rctfd/internal/rctfd-tester:latest"

# Optional: Pick zero or more services to be used on all builds.
# Only needed when using a docker container to run your tests in.
# Check out: http://docs.gitlab.com/ce/ci/docker/using_docker_images.html#what-is-a-service
# services:
#   - mysql:latest
#   - redis:latest
#   - postgres:latest

stages:
  - build
  - examples
  - test
  - docs
  - deploy

# add more dev stages with each feature and an all feature, if necessary
dev:
  stage: build
  script:
    - cargo build --all-targets

# build all examples, if they exist (which they should)
build-examples:
  stage: examples
  script:
    - cargo build --examples
  only:
    changes:
      - src/**/*
      - examples/**/*
      - Cargo.toml
      - Cargo.lock
      - .gitlab-ci.yml

# unit tests
test:
  stage: test
  script:
    - cargo test --all-targets --all-features

# linter
clippy:
  stage: test
  script:
    - cargo clippy --all-targets --all-features -- -D warnings
  only:
    changes:
      - src/**/*
      - Cargo.toml
      - Cargo.lock
      - .gitlab-ci.yml

# auditor
audit:
  stage: test
  script:
    - cargo audit

# benchmark testing
#bench:
#  stage: test
#  script:
#    - cargo bench --all-targets --all-features
#  only:
#    changes:
#      - src/**/*
#      - bench/**/*
#      - Cargo.toml
#      - Cargo.lock
#      - .gitlab-ci.yml

# run all examples, if they exist (which they should)
examples:
  stage: test
  script:
    - for EXAMPLE in $(cargo run --example 2>&1 | grep '    ' | awk '{print $1}'); do echo $EXAMPLE:; cargo run --example $EXAMPLE || exit 1; done
  only:
    changes:
      - src/**/*
      - examples/**/*
      - Cargo.toml
      - Cargo.lock
      - .gitlab-ci.yml

# coverage!
coverage:
  stage: test
  script:
    - cargo tarpaulin --all-features --run-types Tests Examples # Doctests

# developer documentation
devdocs:
  stage: docs
  cache: {}
  artifacts:
    paths:
      - target/doc
  script:
    - cargo doc --all-features --verbose
  only:
    changes:
      - src/**/*
      - Cargo.toml
      - Cargo.lock
      - docs/**/*
      - .gitlab-ci.yml

# user documentation
userdocs:
  stage: docs
  cache: {}
  artifacts:
    paths:
      - docs/book
  script:
    - cd docs; mdbook build
  only:
    changes:
      - src/**/*
      - Cargo.toml
      - Cargo.lock
      - docs/**/*
      - .gitlab-ci.yml

#release:
#  stage: deploy
#  cache: {}
#  script:
#    - cargo build --all-targets --release --verbose
#  # really a nonsensical artifact here, but demonstrates the point
#  artifacts:
#    paths:
#      - target/release/librctfd_template.rlib
#  only:
#    refs:
#      - master
#    changes:
#      - src/**/*
#      - Cargo.toml
#      - Cargo.lock
#      - .gitlab-ci.yml

pages:
  image: alpine:latest
  stage: deploy
  cache: {}
  dependencies:
    - devdocs
    - userdocs
  script:
    - rm -rf public
    - cp -r docs/book public
    - cp -r target/doc/* public/dev/
  artifacts:
    paths:
      - public
  only:
    refs:
      - master
    changes:
      - src/**/*
      - Cargo.toml
      - Cargo.lock
      - docs/**/*
      - .gitlab-ci.yml

cache:
  paths:
    - target/
