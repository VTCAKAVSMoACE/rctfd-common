# RCTFd Common

[![pipeline status](https://gitlab.com/rctfd/core/rctfd-common/badges/master/pipeline.svg)](https://gitlab.com/rctfd/core/rctfd-common/-/commits/master)
[![coverage report](https://gitlab.com/rctfd/core/rctfd-common/badges/master/coverage.svg)](https://gitlab.com/rctfd/core/rctfd-common/-/commits/master)

The common types and their methods required to develop with RCTFd in model, view, and controller.

## Version (vX.X.X) Release Notes

- A brief rundown of changes introduced in this release.
- This should cover all major changes and bugfixes.
- Include any details that would compromise backwards compatibility.
- Make sure these are replicated (and retained) in [CHANGELOG](CHANGELOG.md)

## Documentation

[Documentation][docs] has been made available for this repository.

[docs]: https://rctfd.gitlab.io/core/rctfd-common/

## Developer Dependencies

To develop this software, you'll need to install rust via rustup, then:
 - `rustup component add clippy`
 - `cargo install cargo-audit`
 - `cargo install mdbook`
 - `./hooks/init-hooks.sh`

This way, any contributions you make will be checked before you even submit a
pull request!
