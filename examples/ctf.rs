//! Example which demonstrates the combined usage of the CTF type with the databases.

#![forbid(unsafe_code)]
#![deny(
    missing_debug_implementations,
    missing_docs,
    trivial_casts,
    trivial_numeric_casts,
    unused_extern_crates,
    unused_import_braces,
    unused_qualifications,
    unused_results,
    warnings
)]

#[path = "challenge_db.rs"]
pub mod challenge_db;
#[path = "user_db.rs"]
pub mod user_db;

pub use example::MyCTF;
use rctfd_common::{UserDatabase, CTF};
use std::error::Error;
use std::ops::Deref;
use user_db::StaticSolver;

mod example {
    use super::challenge_db::MyChallengeDB;
    use super::user_db::{MyUserDB, StaticSolver};
    use async_std::sync::{Arc, Mutex};
    use async_trait::async_trait;
    use rctfd_common::CTF;

    /// CTF implementation struct -- fun for the whole family!
    #[derive(Debug, Default)]
    pub struct MyCTF {
        challenge_db: MyChallengeDB,
        solvers: Vec<Arc<Mutex<StaticSolver>>>,
        user_db: MyUserDB,
    }

    #[async_trait]
    impl CTF for MyCTF {
        type UserDatabase = MyUserDB;
        type ChallengeDatabase = MyChallengeDB;
        type Solver = StaticSolver;

        fn user_db(&self) -> &MyUserDB {
            &self.user_db
        }

        fn user_db_mut(&mut self) -> &mut MyUserDB {
            &mut self.user_db
        }

        fn challenge_db(&self) -> &MyChallengeDB {
            &self.challenge_db
        }

        fn challenge_db_mut(&mut self) -> &mut MyChallengeDB {
            &mut self.challenge_db
        }

        fn solvers(&self) -> Vec<Arc<Mutex<StaticSolver>>> {
            self.solvers.clone()
        }
    }

    impl MyCTF {
        /// Creates a new instance of MyCTF.
        pub fn new() -> Self {
            MyCTF::default()
        }
        /// Allows the addition of another solver.
        pub fn add_solver(&mut self, solver: StaticSolver) {
            self.solvers.push(Arc::new(Mutex::new(solver)));
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // create a solver with a single challenge
    let mut solver = StaticSolver::new();
    solver.add_solution(b"correct".to_vec().into_boxed_slice(), 0);
    let mut ctf = MyCTF::new();
    ctf.add_solver(solver);
    // see respective examples user_db and challenge_db for correctness tests on relevant databases

    let mut billy = ctf.user_db_mut().new_user("billy").await?;
    let solvers = ctf.solvers();
    let mut solves = Vec::new();
    for solver in solvers {
        let res = ctf
            .user_db_mut()
            .solve(billy.id(), solver.lock().await.deref(), b"correct")
            .await?;
        billy = res.0;
        solves.extend_from_slice(res.1.as_slice());
    }

    assert_eq!(1, billy.solves().len());
    assert_eq!(0, billy.solves()[0]);
    assert_eq!(0, solves[0]);

    Ok(())
}
