use crate::{ChallengeDatabase, Solver, UserDatabase};
use async_std::sync::{Arc, Mutex};

/// Trait which identifies operations available to a CTF.
pub trait CTF: Send + Sync {
    /// The type of user database employed by this CTF
    type UserDatabase: UserDatabase;
    /// The type of challenge database employed by this CTF
    type ChallengeDatabase: ChallengeDatabase;
    /// The type of solver used by this CTF
    type Solver: Solver + Sized;
    /// Fetches a reference to the user database instance associated with this CTF.
    fn user_db(&self) -> &Self::UserDatabase;
    /// Fetches a mutable reference to the user database instance associated with this CTF.
    fn user_db_mut(&mut self) -> &mut Self::UserDatabase;
    /// Fetches a reference to the challenge database instance associated with this CTF.
    fn challenge_db(&self) -> &Self::ChallengeDatabase;
    /// Fetches a mutable reference to the challenge database instance associated with this CTF.
    fn challenge_db_mut(&mut self) -> &mut Self::ChallengeDatabase;
    /// Fetches the solvers used by this CTF instance.
    fn solvers(&self) -> Vec<Arc<Mutex<Self::Solver>>>;
}
