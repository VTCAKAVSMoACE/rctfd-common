//! Common dependencies for all projects which involve RCTFd.
//!
//! Ensure that you are using this as part of your plugins and controller designs.

#![forbid(unsafe_code)]
#![deny(
    missing_debug_implementations,
    missing_docs,
    trivial_casts,
    trivial_numeric_casts,
    unused_extern_crates,
    unused_import_braces,
    unused_qualifications,
    unused_results,
    warnings
)]

mod challenge;
mod ctf;
mod user;

pub use challenge::*;
pub use ctf::*;
pub use user::*;
