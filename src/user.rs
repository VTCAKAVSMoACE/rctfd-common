use crate::{ChallengeID, Solver};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::cmp::Ordering;
use std::error::Error;

/// ID which represents a user.
pub type UserID = u64;

/// User type which will be commonly used between each MVC component and plugin.
#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct User {
    id: UserID,
    name: String,
    solves: Vec<ChallengeID>,
}

/// Database which controls the mutability of users. This should be implemented as the bridge
/// between view, controller, and model such that the modifications to the user at any location are
/// reflected back to the model.
#[async_trait]
pub trait UserDatabase: Send + Sync {
    /// The error type employed by this challenge database
    type DBError: Error + Clone;
    /// Creates a new user in the database.
    async fn new_user(&mut self, name: &str) -> Result<User, Self::DBError>;

    /// Gets a user by ID.
    async fn user(&self, id: UserID) -> Result<User, Self::DBError>;

    /// Solves challenges for the specified user with the specified solver and flag. Returns the
    /// modified user and a copy of the challenge ids that were solved using this solver and flag.
    async fn solve(
        &mut self,
        id: UserID,
        solver: &dyn Solver,
        flag: &[u8],
    ) -> Result<(User, Vec<ChallengeID>), Self::DBError>;
}

impl User {
    /// Fetches the id for this user.
    #[must_use]
    pub fn id(&self) -> UserID {
        self.id
    }

    /// Fetches the name of this user.
    #[must_use]
    pub fn name(&self) -> &String {
        &self.name
    }

    /// Collects the solves that this user has.
    #[must_use]
    pub fn solves(&self) -> &Vec<ChallengeID> {
        &self.solves
    }

    /// Generates a User instance with the specified values. This will likely only need to be used
    /// by database types.
    #[must_use]
    pub fn new(id: UserID, name: &str, solves: Vec<u64>) -> Self {
        User {
            id,
            name: name.to_string(),
            solves,
        }
    }
}

impl PartialEq for User {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl PartialOrd for User {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.id.partial_cmp(&other.id)
    }
}
