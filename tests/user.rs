//! Test the basic components of the user type.

#![forbid(unsafe_code)]
#![deny(
    missing_debug_implementations,
    missing_docs,
    trivial_casts,
    trivial_numeric_casts,
    unused_extern_crates,
    unused_import_braces,
    unused_qualifications,
    unused_results,
    warnings
)]

use rand::random;
use rctfd_common::User;

#[test]
fn test_id() {
    let id = random();
    let user = User::new(id, "", Vec::new());
    assert_eq!(id, user.id());
}

#[test]
fn test_name() {
    let user = User::new(0, "billy bob", Vec::new());
    assert_eq!("billy bob", user.name());
}

#[test]
fn test_solves() {
    let user = User::new(0, "", vec![1, 2, 2, 3, 3, 3]);
    assert_eq!(1, user.solves().iter().filter(|id| id == &&1).count());
    assert_eq!(2, user.solves().iter().filter(|id| id == &&2).count());
    assert_eq!(3, user.solves().iter().filter(|id| id == &&3).count());
}

#[test]
fn test_eq() {
    let id = random();
    let user1 = User::new(id, "", Vec::new());
    let user2 = User::new(id, "", Vec::new());
    assert_eq!(id, user1.id());
    assert_eq!(id, user2.id());
    assert_eq!(user1, user2);
}

#[test]
fn test_ord() {
    let id = random::<u32>() as u64; // don't overflow, please
    let user1 = User::new(id, "", Vec::new());
    let user2 = User::new(id + 1, "", Vec::new());
    assert_eq!(id, user1.id());
    assert_eq!(id + 1, user2.id());
    assert!(user1 < user2);
}
